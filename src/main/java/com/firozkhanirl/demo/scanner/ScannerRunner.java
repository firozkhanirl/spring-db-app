package com.firozkhanirl.demo.scanner;

import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.firozkhanirl.demo.entity.Address;
import com.firozkhanirl.demo.entity.Person;
import com.firozkhanirl.demo.repository.AddressRepository;
import com.firozkhanirl.demo.repository.PersonRepository;

@Component
public class ScannerRunner implements CommandLineRunner {

	@Autowired
	PersonRepository personRepository;

	@Autowired
	AddressRepository addressRepository;

	private Scanner scanner = new Scanner(System.in);

	private String consoleMessage = "---------------------------------------------------------------------\n"
			+ "Please enter an option\n" + "1. Add Person\n" + "2. Edit Person\n" + "3. Delete Person\n"
			+ "4. Add Address to person\n" + "5. Edit Address\n" + "6. Delete Address\n"
			+ "7. Count Number of Persons\n" + "8. List Persons\n" + "9. Exit\n" + ":";

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Enter option!");
		scanOption();
		scanner.close();
	}

	private void scanOption() {
		System.out.println(consoleMessage);
		if (scanner.hasNextInt()) {
			int option = scanner.nextInt();
			System.out.println("Received input::" + option);

			switch (option) {
			case 1:
				addPerson();
				break;
			case 2:
				editPerson();
				break;
			case 3:
				deletePerson();
				break;
			case 4:
				addAddressToPerson();
				break;
			case 5:
				editAddress();
				break;
			case 6:
				deleteAddress();
				break;
			case 7:
				countPersons();
				break;
			case 8:
				listPersons();
				break;
			case 9:
				scanner.close();
				System.exit(0);
				break;
			default:
				System.out.println("Invalid input. Please enter options (1-9)");
				// scanner.close();
				System.out.println(consoleMessage);
			}
		} else {
			System.out.println("Invalid input. Please enter options (1-9)");
			System.out.println(consoleMessage);
		}
	}

	private String scanString(String fieldName) {
		System.out.println("\t Enter " + fieldName + "::");
		return scanner.next();
	}

	private void addPerson() {
		String firstName = scanString("FirstName");
		String lastName = scanString("LastName");

		System.out.println("Adding Person :: " + firstName + " " + lastName);
		Person person = new Person();
		person.setFirstName(firstName);
		person.setLastName(lastName);
		Person result = personRepository.save(person);
		System.out.println("Saved:: " + result);

		scanOption();
	}

	private void editPerson() {
		System.out.println("Enter Person Id::");
		Long id = scanner.nextLong();
		Optional<Person> find = personRepository.findById(id);

		if (!find.isPresent()) {
			System.out.println("Person with Id::" + id + ", is not found");
		} else {
			System.out.println("Person:: " + find.get());
			Person found = find.get();

			String firstName = scanString("FirstName");
			String lastName = scanString("LastName");

			found.setFirstName(firstName);
			found.setLastName(lastName);
			Person result = personRepository.save(found);
			System.out.println("Saved:: " + result);
		}

		scanOption();
	}

	private void deletePerson() {
		System.out.println("Enter Person Id::");
		Long id = scanner.nextLong();
		Optional<Person> find = personRepository.findById(id);

		if (!find.isPresent()) {
			System.out.println("Person with Id::" + id + ", is not found");
		} else {
			Person found = find.get();
			personRepository.delete(found);
			System.out.println("Deleted :: " + found);
		}

		scanOption();
	}

	private void countPersons() {
		long count = personRepository.count();
		if (count > 0) {
			System.out.println("No.of persons in database ::" + count);
		} else {
			System.out.println("Person database is empty");
		}

		scanOption();
	}

	private void listPersons() {
		long count = personRepository.count();
		if (count > 0) {
			personRepository.findAll().stream().forEach(System.out::println);
		} else {
			System.out.println("Person database is empty");
		}
		scanOption();
	}

	private void addAddressToPerson() {
		System.out.println("Enter Person Id::");
		Long id = scanner.nextLong();
		Optional<Person> find = personRepository.findById(id);

		if (!find.isPresent()) {
			System.out.println("Person with Id::" + id + ", is not found");
		} else {
			System.out.println("Person:: " + find.get());
			Person found = find.get();

			String street = scanString("Address/Street");
			String city = scanString("Address/City");
			String state = scanString("Address/State");
			String postalCode = scanString("Address/PostalCode");

			Address address = new Address();
			address.setStreet(street);
			address.setCity(city);
			address.setState(state);
			address.setPostalCode(postalCode);

			Set<Address> addresses = found.getAddresses();
			addresses.add(address);
			found.setAddresses(addresses);

			Person result = personRepository.save(found);
			System.out.println("Saved:: " + result);
		}

		scanOption();
	}

	private void editAddress() {
		System.out.println("Enter Address Id::");
		Long id = scanner.nextLong();
		Optional<Address> find = addressRepository.findById(id);

		if (!find.isPresent()) {
			System.out.println("Address with Id::" + id + ", is not found");
		} else {
			System.out.println("Address:: " + find.get());
			Address address = find.get();

			String street = scanString("Address/Street");
			String city = scanString("Address/City");
			String state = scanString("Address/State");
			String postalCode = scanString("Address/PostalCode");

			address.setStreet(street);
			address.setCity(city);
			address.setState(state);
			address.setPostalCode(postalCode);

			Address result = addressRepository.save(address);
			System.out.println("Saved:: " + result);
		}

		scanOption();
	}

	private void deleteAddress() {
		System.out.println("Enter Address Id::");
		Long id = scanner.nextLong();
		Optional<Address> find = addressRepository.findById(id);

		if (!find.isPresent()) {
			System.out.println("Address with Id::" + id + ", is not found");
		} else {
			Address found = find.get();
			addressRepository.delete(found);
			System.out.println("Deleted:: " + found);
		}

		scanOption();
	}

}
