package com.firozkhanirl.demo.repository;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.firozkhanirl.demo.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{

}