package com.firozkhanirl.demo.repository;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.firozkhanirl.demo.entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{

}