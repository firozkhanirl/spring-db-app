# spring-db-app

Simple application with database access.

## Check if JAVA_HOME and MAVEN_HOME are set properly
```
$ echo $JAVA_HOME
C:\Program Files\Java\jdk-11.0.8

$ echo $MAVEN_HOME
D:\Software\apache-maven-3.5.4
```

## Clone the application
```
$ git clone https://gitlab.com/firozkhanirl/spring-db-app.git
```

## Build the application
```
$ mvn clean package
```

Application will prompt you for input option
```
Enter option!
---------------------------------------------------------------------
Please enter an option
1. Add Person
2. Edit Person
3. Delete Person
4. Add Address to person
5. Edit Address
6. Delete Address
7. Count Number of Persons
8. List Persons
9. Exit
:
```
Please enter one option for adding/updating/deleting records in database.
